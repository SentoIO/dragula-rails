require 'dragula-rails/version'

module Dragula
  module Rails
    class Engine < ::Rails::Engine
      # Make assets avaiable
    end
  end
end
